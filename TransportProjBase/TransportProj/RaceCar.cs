﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    class RaceCar : Car
    {
        string raceCar = "Race Car";

        public RaceCar(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp(int passengerPos)
        {
            if (YPos < City.YMax)
            {
                //If the passenger or destination is one space away move only one position, if not move 2
                if (FindDifference(YPos, passengerPos) == 1)
                    YPos++;
                else
                    YPos = YPos+2;

                WritePositionToConsole(raceCar);
            }
        }

        public override void MoveDown(int passengerPos)
        {
            if (YPos > 1)
            {
                //If the passenger or destination is one space away move only one position, if not move 2
                if (FindDifference(YPos, passengerPos) == 1)
                    YPos--;
                else
                    YPos = YPos-2;
            }
            else
            {
                YPos--;
            }

            WritePositionToConsole(raceCar);
        }
        
        public override void MoveRight(int passengerPos)
        {
            if (XPos < City.XMax)
            {
                //If the passenger or destination is one space away move only one position, if not move 2
                if (FindDifference(XPos, passengerPos) == 1)
                    XPos++;
                else
                    XPos = XPos+2;

                WritePositionToConsole(raceCar);
            }
        }

        public override void MoveLeft(int passengerPos)
        {
            if (XPos > 1)
            {
                //If the passenger or destination is one space away move only one position, if not move 2
                if (FindDifference(XPos, passengerPos) == 1)
                    XPos--;
                else
                    XPos = XPos-2;
            }
            else
            {
                XPos--;
            }

            WritePositionToConsole(raceCar);
        }

        public int FindDifference(int xPos, int passengerPos)
        {
            //Find difference between car position and passenger position
            return Math.Abs(XPos - passengerPos);
        }

        //protected override void WritePositionToConsole()
        //{
        //    Console.WriteLine(String.Format("Race Car moved to x - {0} y - {1}", XPos, YPos));
        //}
    }
}