﻿using System;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;
            bool hurry = true; //sedan or raceCar

            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), hurry);
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            while(!passenger.IsAtDestination())
            {
                Tick(car, passenger);
            }

            passenger.GetOutOfCar();
            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            //if the passenger has not been picked up, go to starting passenger position and move car
            if ((car.XPos != passenger.StartingXPos || car.YPos != passenger.StartingYPos) && passenger.passengerPickedUp == false)
            {
                #region Pickup
                
                if (car.XPos < passenger.StartingXPos)
                    car.MoveRight(passenger.StartingXPos);
                else if (car.XPos != passenger.StartingXPos)
                    car.MoveLeft(passenger.StartingXPos);

                if (car.YPos < passenger.StartingYPos)
                    car.MoveUp(passenger.StartingYPos);
                else if (car.YPos != passenger.StartingYPos)
                    car.MoveDown(passenger.StartingYPos);
                
                #endregion
            }
            else 
            {
                //if the car is at Starting Position, picked up passenger
                if(car.Passenger == null)
                {
                    car.PickupPassenger(passenger);
                    passenger.GetInCar(car);
                    passenger.passengerPickedUp = true;
                }

                //if the car is not at the destination position, move the car to drop off position
                if (car.XPos != passenger.DestinationXPos || car.YPos != passenger.DestinationYPos)
                {
                    #region Dropoff

                    if (car.XPos < passenger.DestinationXPos)
                       car.MoveRight(passenger.DestinationXPos);
                    else if (car.XPos != passenger.DestinationXPos)
                        car.MoveLeft(passenger.DestinationXPos);

                    if (car.YPos < passenger.DestinationYPos)
                        car.MoveUp(passenger.DestinationYPos);
                    else if (car.YPos != passenger.DestinationYPos)
                        car.MoveDown(passenger.DestinationYPos);
                    
                    #endregion
                }
            }
        }
    }
}
