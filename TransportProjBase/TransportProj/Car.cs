﻿using System;

namespace TransportProj
{
    public abstract class Car
    {
        #region Get/Set

        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }
        
        #endregion

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
        }

        protected virtual void WritePositionToConsole(String carType)
        {
            Console.WriteLine(String.Format(carType + " moved to x - {0} y - {1}", XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        public abstract void MoveUp(int passengerPos);

        public abstract void MoveDown(int passengerPos);

        public abstract void MoveRight(int passengerPos);

        public abstract void MoveLeft(int passengerPos);
    }
}
