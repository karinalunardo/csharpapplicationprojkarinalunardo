﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        string sedan = "Sedan";

        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp(int passengerPos)
        {
            if (YPos < City.YMax)
            {
                YPos++;
                WritePositionToConsole(sedan);
            }
        }

        public override void MoveDown(int passengerPos)
        {
            if (YPos > 0)
            {
                YPos--;
                WritePositionToConsole(sedan);
            }
        }

        public override void MoveRight(int passengerPos)
        {
            if (XPos < City.XMax)
            {
                XPos++;
                WritePositionToConsole(sedan);
            }
        }

        public override void MoveLeft(int passengerPos)
        {
            if (XPos > 0)
            {
                XPos--;
                WritePositionToConsole(sedan);
            }
        }

        //protected override void WritePositionToConsole()
        //{
        //    Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        //}
    }
}
